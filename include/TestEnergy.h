// This file is part of GOAST, a C++ library for variational methods in Geometry Processing
//
// Copyright (C) 2020 Behrend Heeren & Josua Sassen, University of Bonn <goast@ins.uni-bonn.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
/**
 * \file
 * \brief A simple test energy.
 */

#ifndef GOASTEXAMPLE_TESTENERGY_H
#define GOASTEXAMPLE_TESTENERGY_H

/**
 * \brief A simple test energy.
 *
 * This implements the Rosenbrocks energy
 * f(x,y) = 100 * (y - x^2)^2 + (1-x)^2
 */
template<typename ConfiguratorType>
class TestEnergy : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::RealType> {
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;

public:
  void apply( const VectorType &Arg, RealType &Dest ) const {
    RealType aux1 = Arg[1] - Arg[0] * Arg[0];
    RealType aux2 = 1 - Arg[0];
    Dest = (100 * aux1 * aux1 + aux2 * aux2);
  }
};

/**
 * \brief Gradient of the test energy.
 * \sa TestEnergy
 */
template<typename ConfiguratorType>
class TestGradient : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::VectorType> {
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;

public:
  void apply( const VectorType &Arg, VectorType &Dest ) const {
    if ( Dest.size() != Arg.size())
      Dest.resize( Arg.size());
    Dest.setZero();
    RealType aux1 = Arg[1] - Arg[0] * Arg[0];
    RealType aux2 = 1 - Arg[0];
    Dest[0] -= (400 * aux1 * Arg[0] + 2 * aux2);
    Dest[1] += (200 * aux1);
  }
};

/**
 * \brief Hessian of the test energy.
 * \sa TestEnergy
 */
template<typename ConfiguratorType>
class TestHessian : public BaseOp<typename ConfiguratorType::VectorType, typename ConfiguratorType::SparseMatrixType> {
  typedef typename ConfiguratorType::RealType RealType;
  typedef typename ConfiguratorType::VectorType VectorType;
  typedef typename ConfiguratorType::SparseMatrixType MatrixType;
public:
  void apply( const VectorType &Arg, MatrixType &Dest ) const {
    if ((Dest.rows() != 2) || (Dest.cols() != 2))
      Dest.resize( 2, 2 );
    Dest.setZero();

    RealType aux1 = Arg[1] - Arg[0] * Arg[0];
    Dest.coeffRef( 0, 0 ) += (800 * Arg[0] * Arg[0] - 400 * aux1 + 2);
    Dest.coeffRef( 0, 1 ) -= (400 * Arg[0]);
    Dest.coeffRef( 1, 0 ) -= (400 * Arg[0]);
    Dest.coeffRef( 1, 1 ) += (200.);
  }
};
//================================================================================

#endif //GOASTEXAMPLE_TESTENERGY_H
