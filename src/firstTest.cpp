// This file is part of GOAST, a C++ library for variational methods in Geometry Processing
//
// Copyright (C) 2020 Behrend Heeren & Josua Sassen, University of Bonn <goast@ins.uni-bonn.de>
//
// This Source Code Form is subject to the terms of the Mozilla
// Public License v. 2.0. If a copy of the MPL was not distributed
// with this file, You can obtain one at http://mozilla.org/MPL/2.0/.
// system includes
#include <iostream>
#include <string>

// GOAST includes
#include <goast/Core.h>

// own includes
#include "TestEnergy.h"

//================================================================================

typedef DefaultConfigurator ConfiguratorType;

typedef typename ConfiguratorType::RealType RealType;
typedef typename ConfiguratorType::VectorType VectorType;
typedef typename ConfiguratorType::SparseMatrixType MatrixType;
typedef typename ConfiguratorType::TripletType TripletType;
typedef typename ConfiguratorType::VecType VecType;
typedef std::vector<TripletType> TripletListType;


//#################################
//#################################
int main( int argc, char *argv[] ) {
  try {
    std::cerr << "=====================================================" << std::endl;
    std::cerr << "Start mesh example..." << std::endl;
    TriMesh mesh;

    OpenMesh::IO::read_mesh( mesh, "../data/finger0.ply" );
    MeshTopologySaver topology( mesh );
    std::cerr << "Mesh has " << topology.getNumVertices() << " vertices, " << topology.getNumFaces() << " faces and "
              << topology.getNumEdges() << " edges!" << std::endl;


    VectorType geometry;
    getGeometry( mesh, geometry );
    VecType node;
    getXYZCoord<VectorType, VecType>( geometry, node, 0 );
    std::cerr << "The first node is given by  " << node << std::endl;

    std::cerr << "=====================================================" << std::endl;
    std::cerr << "Start energy example..." << std::endl;
    TestEnergy<ConfiguratorType> E;
    TestGradient<ConfiguratorType> DE;
    TestHessian<ConfiguratorType> D2E;

    VectorType start( 2 ), solution( 2 );
    start[0] = 1.2;
    start[1] = 1.2;

    std::cerr << "Start first derivative test..." << std::endl;
    ScalarValuedDerivativeTester<ConfiguratorType>( E, DE, 1e-8 ).plotAllDirections( start, "gradTest" );

    std::cerr << "Start second derivative test..." << std::endl;
    VectorValuedDerivativeTester<ConfiguratorType>( DE, D2E, 1e-5 ).plotAllDirections( start, "hessTest" );


    std::cerr << "=====================================================" << std::endl;
    std::cerr << "Start optimization example..." << std::endl;

    std::cerr << "Start gradient descent with Armijo... " << std::endl;
    GradientDescent<ConfiguratorType> gdSolver( E, DE, 1000, 1e-8, ARMIJO, SHOW_ALL );
    gdSolver.solve( start, solution );
    std::cerr << solution << std::endl << std::endl;

    std::cerr << "=====================================================" << std::endl;
    std::cerr << "Start matrix example..." << std::endl;


    int num = 7;
    MatrixType mat( num, num );
    VectorType vec( num ), sol( num );

    std::vector<TripletType> tripletList;
    for ( int i = 0; i < num; i++ ) {
      tripletList.push_back( TripletType( i, i, i + 1 ));
      vec[i] = 1.;
    }
    mat.setFromTriplets( tripletList.cbegin(), tripletList.cend());

    std::cerr << mat << std::endl;

#ifdef USE_SOLVER_LIB
    LinearSolver<ConfiguratorType> LUSolver;
    LUSolver.prepareSolver( mat );
    LUSolver.backSubstitute( vec, sol );

    std::cout << sol << std::endl;
#endif

  }
  catch ( BasicException &el ) {
    std::cerr << std::endl << "ERROR!! CAUGHT FOLLOWING EXECEPTION: " << std::endl << el.getMessage() << std::endl
              << std::flush;
  }

  return 0;
}