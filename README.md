# GOAST example project
A blank project example showcasing how to use [GOAST 👻](https://gitlab.com/numod/goast) as git submodule in a CMake project. 
Feel free to copy or fork this project as a way of starting a new project with GOAST!


[[_TOC_]]

## Using this project
### Cloning / Git submodule
GOAST is used in this project as a git submodule located in ``deps/GOAST``. 
This means to clone the repository one has to use either

```bash
git clone https://gitlab.com/numod/goast-example-project.git 
cd goast-example-project # or which ever folder you cloned the project to
git submodule init # initialize submodules given in .gitmodules
git submodule update # clone submodules
```

or, more compactly,
```bash
git clone --recurse-submodules https://gitlab.com/numod/goast-example-project.git # --recursive in older git versions
```
For a detailed explanation of git submodules see for example the [git book](https://git-scm.com/book/en/v2/Git-Tools-Submodules).

### Dependencies
The only dependencies are the ones inherited from GOAST, i.e. 
 - Eigen, 
 - OpenMesh, and 
 - SuiteSparse.

### Compiling
The project is compiled using the common CMake routine:
```bash
mkdir build
cd build
cmake ..
make all
```
Depending on how you installed the dependencies this might not find all of them. 
In this case, you will need to tell CMake where they are explicitly by setting the following options.

| Option | Explanation | Example value |
| ---      |  ------  |---------:|
| `SUITESPARSE_INCLUDE_DIR_HINTS`   | `include` directory of SuiteSparse  | `/opt/suitesparse/lib`   |
| `SUITESPARSE_LIBRARY_DIR_HINTS`   | `lib` directory of SuiteSparse  | `/opt/suitesparse/include`   |
| `EIGEN3_INCLUDE_DIR`   | Directory of your Eigen installation  | `/usr/include/eigen3`   |
| `OPENMESH_LIBRARY_DIR`  | Directory of your OpenMesh installation  | `/usr/local`   |

This can be achieved by specifying them in the terminal in the following way
```bash
cmake -D<OPTION>=<VALUE> ..
```
or by using `cmake-gui`.

### Running
In `build` directory created above simply execute:
```bash
./firstExample
```

This should print some tests confirming the basic functionality of GOAST👻.    

## Behind the scenes
### Folder structure
 - **data** basic data used in the project
 - **deps** (external) dependencies of the project, especially GOAST
 - **include** headers with classes etc. produced for the project, in this cased only a simple energy functional
 - **src** source code of the exectuables/libraries which belong to the project

### CMake Setup
In this project, we use the CMake module provided for GOAST in the main repository.
This means we do not have to specify explicit find commands for the dependencies but can simply use the compact code.
```cmake
list(APPEND CMAKE_MODULE_PATH ${CMAKE_SOURCE_DIR}/deps/GOAST/cmake) # path to GOAST module
include(GOAST) # load GOAST module

target_link_libraries(<YOUR_TARGET> PUBLIC goast::goast)
```

Furthermore, we have included examples of how to enable OpenMP and architecture optimizations in the project.

### Updating submodule & pulling
In general, when you clone this project the GOAST submodule might be outdated.
To update all submodules in the project you can use
```bash
git submodule update --remote
```
Note that you will have to commit the change in the submodule!
If you pull changes from the project remote which update the submodule you have to run
```bash
git submodule update --init --recursive # options are just there to be safe
```
to fully receive the updates.
Again, we advise you to read the [git book](https://git-scm.com/book/en/v2/Git-Tools-Submodules) (or another tutorial) if you intent to use GOAST as a git submodule.
